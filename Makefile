init: api-clear docker-down docker-pull docker-build docker-up api-permissions
up: docker-up api-permissions
down: docker-down
restart: down up
test: api-test api-fixtures
test-unit: api-test-unit
test-func: api-test-functional api-fixtures
test-unit-coverage: api-test-unit-coverage
test-func-coverage: api-test-functional-coverage api-fixtures
validate-schema: api-validate-schema

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

api-init: api-composer-install

api-composer-install:
	docker-compose run --rm api-php-cli composer install

api-test:
	docker-compose run --rm api-php-cli composer test

api-test-unit:
	docker-compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-functional:
	docker-compose run --rm api-php-cli composer test -- --testsuite=functional

api-test-unit-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=functional

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/*'

api-migrations:
	docker-compose run --rm api-php-cli composer app migrations:migrate

api-fixtures:
	docker-compose run --rm api-php-cli composer app fixtures:load

api-validate-schema:
	docker-compose run --rm api-php-cli composer app orm:validate-schema

api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod -R 777 var vendor