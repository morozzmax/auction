<?php
return [
    \Psr\Log\LoggerInterface::class=>function(\Psr\Container\ContainerInterface $container){
        $config = $container->get('config')['logger'];
        $level = $config['debug'] ? \Monolog\Logger::DEBUG : \Monolog\Logger::INFO;
        $log = new \Monolog\Logger('API');
        if($config['stderr']){
            $log->pushHandler(new \Monolog\Handler\StreamHandler('php://stderr',$level));
        }
        if(!empty($config['file'])){
            $log->pushHandler(new \Monolog\Handler\StreamHandler($config['file'],$level));
        }
        return $log;
    },
    'config'=>[
        'logger'=>[
            'debug'=> (bool)getenv('APP_DEBUG'),
            'file'=>null,
            'stderr'=>true,
        ]
    ]
];