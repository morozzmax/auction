<?php

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\Translation\TranslatorInterface;

return [
    ValidatorInterface::class => function (ContainerInterface $container): ValidatorInterface
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');

        /** @var TranslatorInterface $translator */
        $translator = $container->get(TranslatorInterface::class);

        return  Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->setTranslator($translator)
            ->setTranslationDomain('validators')
            ->getValidator();
    }
];
