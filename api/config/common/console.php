<?php
use Doctrine\Migrations;
use Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand;

return [
    'config'=>[
        'console'=>[
            'commands'=>[
                \App\Console\HelloCommand::class,
                \App\Console\MailerCheckCommand::class,
                \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand::class,

                Migrations\Tools\Console\Command\ExecuteCommand::class,
                Migrations\Tools\Console\Command\MigrateCommand::class,
                Migrations\Tools\Console\Command\LatestCommand::class,
                Migrations\Tools\Console\Command\StatusCommand::class,
                Migrations\Tools\Console\Command\UpToDateCommand::class,
            ]
        ],
    ],
];
