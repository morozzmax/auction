<?php
//$files = glob(__DIR__.'/common/*.php');
/*$files = array_merge(
    glob(__DIR__.'/common/*.php') ?: [],
    glob(__DIR__.'/'.(getenv('APP_ENV') ?: 'prod').'/*.php') ?: []
);


$configs = array_map(
    static function ($file){
        return require $file;
    },
    $files
);

return array_merge_recursive(...$configs);*/

use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\ConfigAggregator\PhpFileProvider;

$aggregator = new ConfigAggregator([
    new PhpFileProvider(__DIR__ . '/common/*.php'),
    new PhpFileProvider(__DIR__ . '/' . (getenv('APP_ENV') ?: 'prod') . '/*.php'),
]);

return $aggregator->getMergedConfig();
