<?php

use Slim\App;
use Slim\Factory\AppFactory;

return static function (\Psr\Container\ContainerInterface $container): App {
    $app = AppFactory::createFromContainer($container);
    (require __DIR__.'/middleware.php')($app, $container);
    (require __DIR__.'/routes.php')($app);
    return  $app;
};