<?php

use App\Http\Middleware\ClearEmptyInput;
use App\Http\Middleware\DomainExceptionHandler;
use App\Http\Middleware;
use Middlewares\ContentLanguage;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use App\Http\Middleware\ValidationExeptionHandler;

return static function (App $app): void {
    $app->add(ValidationExeptionHandler::class);
    $app->add(DomainExceptionHandler::class);
    $app->add(ClearEmptyInput::class);
    $app->add(Middleware\TranslatorLocale::class);
    $app->add(ContentLanguage::class);
    $app->addBodyParsingMiddleware();
    $app->add(ErrorMiddleware::class);
};