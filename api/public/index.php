<?php

use Slim\Factory\AppFactory;

http_response_code(500);

require __DIR__ . '/../vendor/autoload.php';

//Sentry\init(['dsn' => 'https://87bb2abc8dba4e20b35453db0f4e891d@o390354.ingest.sentry.io/5233717' ]);

$container = require __DIR__.'/../config/container.php';

$app = (require __DIR__.'/../config/app.php')($container);

$app->run();

