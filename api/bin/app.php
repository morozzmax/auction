#!/usr/bin/env php
<?php

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelper;

require __DIR__ . '/../vendor/autoload.php';

//Sentry\init(['dsn' => 'https://87bb2abc8dba4e20b35453db0f4e891d@o390354.ingest.sentry.io/5233717' ]);

/** @var ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';

$cli = new Application('Console');

/**
 * @var string[] $commands
 * @psalm-suppress MixedArrayAccess
 */
$commands = $container->get('config')['console']['commands'];

/** @var EntityManagerInterface $entityManager */
$entityManager = $container->get(EntityManagerInterface::class);
$connection = $entityManager->getConnection();
$configuration = new Configuration($connection);
$configuration->setMigrationsDirectory(__DIR__ . '/../src/Data/Migration');
$configuration->setMigrationsNamespace('App\Data\Migration');
$configuration->setMigrationsTableName('migrations');
$configuration->setAllOrNothing(true);
$configuration->setCheckDatabasePlatform(false);



$cli->getHelperSet()->set(new EntityManagerHelper($entityManager), 'em');
$cli->getHelperSet()->set(new ConfigurationHelper($connection, $configuration), 'configuration');

//\Doctrine\Migrations\Tools\Console\ConsoleRunner::addCommands($cli);

foreach ($commands as $name) {
    /** @var Command $command */
    $command = $container->get($name);
    $cli->add($command);
}

$cli->run();
